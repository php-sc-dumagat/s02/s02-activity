<?php require_once("./code.php") ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S02: Repetition Control Structures and Array Manipulation Activity</title>
    </head>
    <body>
        
        <h1>Divisibles by Five</h1>
    
        <p><?php whileLoop(); ?></p>

        <h1>Array Manipulation</h1>

        <?php array_push($students, 'John Smith') ?>

        <p><?php var_dump($students) ?></p>

        <p><?php echo count($students) ?></p>

        <?php array_push($students, 'Jane Smith') ?>

        <p><?php var_dump($students) ?></p>

        <p><?php echo count($students) ?></p>

        <?php array_shift($students) ?>

        <p><?php var_dump($students) ?></p>

        <p><?php echo count($students) ?></p>

    </body>
</html>